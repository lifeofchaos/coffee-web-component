import template from './coffee.html';
import coffee from './coffee_mug.svg';


window.addEventListener('WebComponentsReady', function (e) {
    (()=> {
        const templateEl = document.createElement('template');
        templateEl.innerHTML = template;
        class CoffeeElement extends HTMLElement {
            constructor() {
                super();
                templateEl.content.querySelector('.coffee_container').innerHTML = coffee;
                const shadow = this.attachShadow({ mode: 'open' });
                shadow.appendChild(templateEl.content.cloneNode(true));
                this._mug = this.shadowRoot.querySelector('#Mug');
                this._dish = this.shadowRoot.querySelector('#Dish');
                this._foam = this.shadowRoot.querySelector('#Foam');
                this._milk = this.shadowRoot.querySelector('#Milk');
                this._coffee = this.shadowRoot.querySelector('#Coffee');
                this._cocoa = this.shadowRoot.querySelector('#Cocoa');
                this._water = this.shadowRoot.querySelector('#Water');
                this._ice = this.shadowRoot.querySelector('#Ice');
                this.render();
            }
            static get observedAttributes() {
                return [
                    'mug-color',
                    'foam-color',
                    'coffee-color',
                    'cocoa-color',
                    'milk-color',
                    'dish-color',
                    'water-color',
                    'ice-color',
                    'ice-border-color',
                    'coffee',
                    'milk',
                    'water',
                    'cocoa',
                    'ice',
                    'foam'
                ];
            }
            attributeChangedCallback(attr, oldValue, newValue) {
                switch(attr) {
                    case 'mug-color':
                        this._fillMug();
                        break;
                    case 'foam-color':
                        this._fillFoam();
                        break;
                    case 'coffee-color':
                        this._fillCoffee();
                        break;
                    case 'cocoa-color':
                        this._fillCocoa();
                        break;
                    case 'milk-color':
                        this._fillMilk();
                        break;
                    case 'dish-color':
                        this._fillDish();
                        break;
                    case 'water-color':
                        this._fillWater();
                        break;   
                    case 'ice-color':
                    case 'ice-border-color':
                        this._fillIce();
                        break;
                    case 'coffee':
                    case 'cocoa':
                    case 'milk':
                    case 'water':
                    case 'foam':
                    case 'ice':    
                        this._fillFluids();
                        break;    
                }
            }
            render() {
                this._emptyMug();
                this._fillMug();
                this._fillDish();
                this._fillFoam();
                this._fillMilk();
                this._fillCocoa();
                this._fillCoffee();
                this._fillWater();
                this._fillIce();
                this._fillFluids();
            }
            _fillFluids() {
                this._foam.style.transform = `translateY(${this.foam})`;
                this._milk.style.transform = `translateY(${this.milk})`;
                this._coffee.style.transform = `translateY(${this.coffee})`;
                this._cocoa.style.transform = `translateY(${this.cocoa})`;
                this._water.style.transform = `translateY(${this.water})`;
                this._ice.style.display = this.ice;
            }

            _emptyMug() {
                this._coffee.style.transform = 'translateY(80%)';
                this._cocoa.style.transform = 'translateY(80%)';
                this._milk.style.transform = 'translateY(80%)';
                this._water.style.transform = 'translateY(90%)';
                this._foam.style.transform = 'translateY(90%)';
                this._ice.style.display = 'none';
            }

            _fillMug() {
                this._mug.style.fill = this.mugColor;
            }
            _fillFoam() {
                this._foam.style.fill = this.foamColor;
            }
            _fillCoffee() {
                this._coffee.style.fill = this.coffeeColor;
            }
            _fillMilk() {
                this._milk.style.fill = this.milkColor;
            }
            _fillDish() {
                this._dish.style.fill = this.dishColor;
            }
            _fillCocoa() {
                this._cocoa.style.fill = this.cocoaColor;
            }
            _fillWater() {
                this._water.style.fill = this.waterColor;
            }
            _fillIce() {
                this._ice.querySelectorAll('rect').forEach(rect => {
                    rect.style.fill = this.iceColor;
                    rect.style.stroke = this.iceBorderColor;
                });
            }
            get mugColor() {
                return this.hasAttribute('mug-color') ? this.getAttribute('mug-color') : '#000000';
            }
            get foamColor() {
                return this.hasAttribute('foam-color') ? this.getAttribute('foam-color') : '#F2DED5';
            }
            get coffeeColor() {
                return this.hasAttribute('coffee-color') ? this.getAttribute('coffee-color') : '#644842';
            }
            get milkColor() {
                return this.hasAttribute('milk-color') ? this.getAttribute('milk-color') : '#DDCBB4';
            }
            get waterColor() {
                return this.hasAttribute('water-color') ? this.getAttribute('water-color') : '#A6D7E4';
            }
            get dishColor() {
                return this.hasAttribute('dish-color') ? this.getAttribute('dish-color') : '#000000';
            }
            get cocoaColor() {
                return this.hasAttribute('cocoa-color') ? this.getAttribute('cocoa-color') : '#402C28';
            }
            get iceColor() {
                return this.hasAttribute('ice-color') ? this.getAttribute('ice-color') : '#A6D7E4';
            }
            get iceBorderColor() {
                return this.hasAttribute('ice-border-color') ? this.getAttribute('ice-border-color') : '#7BCCE5';
            }
            _calcPosition(attr, maxValue = 80) {
                if (this.hasAttribute(attr) && this.getAttribute(attr)) {
                    return (maxValue - Math.floor(parseInt(this.getAttribute(attr)) * maxValue / 100)) + '%';
                }
                return maxValue;
            }
            get foam() {
                return this._calcPosition('foam', 90);
            }
            get milk() {
                return this._calcPosition('milk');
            }
            get coffee() {
                return this._calcPosition('coffee');
            }
            get cocoa() {
                return this._calcPosition('cocoa');
            }
            get water() {
                return this._calcPosition('water', 90);
            }
            get ice() {
                return this.hasAttribute('ice') && this.getAttribute('ice').toUpperCase() === 'YES' ? 'block' : 'none';
            }
        }
        customElements.define('loc-coffee', CoffeeElement);
    })();
});

