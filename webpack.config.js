const { CleanWebpackPlugin } = require('clean-webpack-plugin');
const HtmlWebpackPlugin = require('html-webpack-plugin')
const path = require('path');

module.exports = env => {
  return ({
    mode: env.MODE,
    entry: {
        coffee: './src/coffee.js',
        webcomponentsloader: './src/webcomponents-loader.js'
    },
    devtool: env.MODE === 'development' ? 'inline-source-map' : 'none',
    devServer: {
      contentBase: './dist',
      host: '127.0.0.1'
    },
    module: {
      rules: [
        {
            test: /\.js$/,
            exclude: '/node_modules/',
            use: {
                loader: 'babel-loader'
            }
        },
        {
            test: /\.css$/i,
            use: ['style-loader', 'css-loader'],
        },
        {
            test: /\.(html)$/,
            use: {
              loader: 'html-loader',
            },
        },
        {
            test: /\.svg$/,
            loader: 'svg-inline-loader'
        }
      ],
    },
    output: {
      filename: '[name].js',
      path: path.resolve(__dirname, 'dist')
    },
    plugins: [
      new CleanWebpackPlugin(),
      new HtmlWebpackPlugin({
        inject: 'head',
        template: 'demo/index.html'
      })
    ]
  });
};